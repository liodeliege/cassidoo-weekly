import java.lang.reflect.Array;
import java.util.ArrayList;
        import java.util.List;

public class FilterGeneric<T> {

    public static void main(String[] args) {
        String [] test = new String[]{"aaaaaa", "aaa", "aaaa", "bbbbbb"};
        FilterGeneric<String> main = new FilterGeneric<>();
        Object[] result = main.filterArray(test, String.class, valueToFilter -> valueToFilter.length() == 6);
        System.out.println(result.length);

    }

    public T[] filterArray(T[] toFilter, Class<T> arrayType, Filter<T> f ){
        if (toFilter == null) return null;

        List<T> result = new ArrayList();
        for (T value : toFilter){
            if (f.BooleanFilter(value)){
                result.add(value);
            }
        }
        return result.toArray((T[])Array.newInstance(arrayType, result.size()));  //Cannot instantiate an T[] array so, Array.newInstance, Class pass as parameter and casting :(...
    }


}

interface Filter<X> {
    boolean BooleanFilter(X valueToFilter);
}