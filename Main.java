import java.util.ArrayList;
import java.util.List;

public class Main {

    public static Object[] filterArray(Object[] toFilter, Filter f ){
        if (toFilter == null) return null;

        List result = new ArrayList();
        for (Object value : toFilter){
            if (f.BooleanFilter(value)){
                result.add(value);
            }
        }
        return result.toArray();
    }

    public static void main(String[] args) {
        String [] test = new String[]{"aaaaaa", "aaa", "aaaa", "bbbbbb"};

        System.out.println(filterArray(test, valueToFilter -> (valueToFilter instanceof String && ((String)valueToFilter).length() == 6)).length);

    }

}


interface Filter {
    boolean BooleanFilter(Object valueToFilter);
}