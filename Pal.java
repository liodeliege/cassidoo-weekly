import javax.persistence.GeneratedValue;
import java.math.BigDecimal;
import java.math.BigInteger;

public class Pal {


    public static final BigInteger NINE = BigInteger.valueOf(9);
    static long odd = 1+2+3+4+5+6+7+8+9;
    static BigInteger oddBig = BigInteger.valueOf(odd);
    static long even = 11+22+33+44+55+66+77+88+99;
    static BigInteger evenBig = BigInteger.valueOf(even);


    /**
     * Generate the structure of the palindrome as:
     * 1xxxx1, 2xxxx2 etc
     *  1xx1,   2xx2 etc
     *
     * Sum the result.
     * if runningCount == 0, first iteration, the sum must be multiply by 10 (all values between 0 and 9)
     * if runningCount != 0, other iterations, the sum must be multiply by 9 and by the 10 power(iteration number).
     *
     * @param digit  number of digit of the palindrome
     * @param runningCount must be equal to 0 (recursion)
     * @param currentValue must be equal to 0 (recursion)
     * @return
     */
    public static long SumPalindrome(int digit, int runningCount, long currentValue){
        if (digit > 12) {
            throw new RuntimeException("cannot generate Palindrome with more than 12 digits, use BigInteger version");
        }
        if (digit == 1) {
           long mask = PowTen(runningCount);
           return currentValue + (odd * mask * 9 *PowTen(runningCount-1));
        } else if (digit == 2){
            long mask = PowTen(runningCount);
            return currentValue + (even * mask)*9*PowTen(runningCount-1);
        } else {
            long intermediate = 0;
            for (int i = 1;  i<10; i++){
                intermediate = intermediate + ((i*( PowTen(digit-1)) + i) * PowTen(runningCount));
            }


            if (runningCount == 0){
                intermediate = intermediate *10;  // initial palindrome, with the 0  x0000000x
            } else {
                intermediate = intermediate * 9 * PowTen(runningCount);
            }

            currentValue = currentValue*10; // for this iteration, we will repeat 10 time the previous iteration

            return SumPalindrome(digit-2, runningCount+1, intermediate+currentValue);
        }
    }

    public static BigInteger SumPalindrome(int digit, int runningCount, BigInteger currentValue){
        if (digit == 1) {
            BigInteger mask = BigInteger.TEN.pow(runningCount);
            return currentValue.add(mask.multiply(oddBig).multiply(NINE).multiply(BigInteger.TEN.pow(runningCount-1)));
        } else if (digit == 2){
            BigInteger mask = BigInteger.TEN.pow(runningCount);
            return currentValue.add(mask.multiply(evenBig).multiply(NINE).multiply(BigInteger.TEN.pow(runningCount-1)));

        } else {
            BigInteger intermediate = BigInteger.ZERO;
            for (int i = 1;  i<10; i++){
                BigInteger palindrome = (BigInteger.TEN.pow(digit-1).multiply(BigInteger.valueOf(i)).add(BigInteger.valueOf(i))).multiply(BigInteger.TEN.pow(runningCount));
                intermediate = intermediate.add(palindrome);
            }


            if (runningCount == 0){
                intermediate = intermediate.multiply(BigInteger.TEN);  // initial palindrome, with the 0  x0000000x
            } else {
                intermediate = intermediate.multiply(BigInteger.valueOf(9)).multiply(BigInteger.TEN.pow(runningCount));
            }

            currentValue = currentValue.multiply(BigInteger.TEN); // for this iteration, we will repeat 10 time the previous iteration

            return SumPalindrome(digit-2, runningCount+1, currentValue.add(intermediate));
        }
    }

    /**
     * Palindrome sum is always equals to 495 with leading 0.
     *
     * for digit > 2, the rule is
     *
     * 3: 00 leading (+2 0)
     * 4: 000 leading (+1 0)
     * 5: 00000 leading (+2 0)
     * 6: 000000 leading (+1 0)
     * 7: 00000000 leading ...
     *
     * @param digit
     * @return
     */
    public static long GenerateSumPalindrome(int digit){
        if (digit > 12) {
            throw new RuntimeException("cannot sum Palindrome with more than 12 digits, use BigInteger version");
        }
        if (digit == 1){
            return odd;
        } else if (digit == 2) {
            return even;
        } else {
            int pow = 0;
            for (int i = 3; i<=digit; i++){
                if (i%2 == 0){
                    pow = pow +1;
                } else {

                    pow = pow +2;
                }
            }
            return 495*PowTen(pow);
        }
    }


    public static BigInteger GenerateSumPalindromeBig(int digit){
        if (digit == 1){
            return oddBig;
        } else if (digit == 2) {
            return evenBig;
        } else {
            int pow = 0;
            for (int i = 3; i<=digit; i++){
                if (i%2 == 0){
                    pow = pow +1;
                } else {

                    pow = pow +2;
                }
            }
            return BigInteger.TEN.pow(pow).multiply(BigInteger.valueOf(495));
        }
    }


    private static long PowTen(long number){

        return Math.round(Math.pow(10,number));
    }

    public static void main(String[] args){
        System.out.println(SumPalindrome(3, 0, 0));
        System.out.println(SumPalindrome(3, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindrome(3));
        System.out.println(SumPalindrome(4, 0, 0));
        System.out.println(SumPalindrome(4, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindrome(4));
        System.out.println(SumPalindrome(5, 0, 0));
        System.out.println(SumPalindrome(5, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindrome(5));
        System.out.println(SumPalindrome(6, 0, 0));
        System.out.println(SumPalindrome(6, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindrome(6));
        System.out.println(SumPalindrome(7, 0, 0));
        System.out.println(SumPalindrome(7, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindrome(7));
        System.out.println(SumPalindrome(12, 0, 0));
        System.out.println(SumPalindrome(12, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindrome(12));



        System.out.println(SumPalindrome(30, 0, BigInteger.ZERO));
        System.out.println(GenerateSumPalindromeBig(30));
    }
    
    
}
